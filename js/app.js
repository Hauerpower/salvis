$(document).foundation()


////animation-home-page
$(document).ready(function () {

    //search-toggle
    $("#search").click(function (e) {
        e.preventDefault()
        $(".links-box input[type=search]").slideToggle();
    });
    // $("body").not(".links-box input[type=search]").click(function (e) {
    //     $(".links-box input[type=search]").hide();
    // });


    //arrow on main baner
    $(' .arrow-down-triangle img').click(function () {
        $('html,body').animate({
            scrollTop: $('.owl-1').offset().top
        }, 500, "easeOutCirc");
        //To use more animation, like this above, you need easing plugin. In this case is: jQuery Easing Plugin (version 1.3)
    });


    ////animacja 3-steps (main page)
    $(' .shadow-ball').click(function (e) {
        e.preventDefault()
        let element_id = $(this).attr('data-123-anim');
        let target = "#step-" + element_id;
        $(".animated-elements").hide();
        $(target).css("display", "flex");
        $("#animated-img").css("margin-left", "-200px")
        $("#animated-img").removeClass('add-transform');
        // $(".animated-elements").addClass('display-flex-anim');

    })

    //////////arrow-down, toggle - front_page-mobile
    $(".step-box").click(function () {
        let id_faq = $(this).attr('data-value-step');
        let target = '.faq-' + id_faq;
        if ($(".step-box-text:visible").not($(target)).length) {
            $(".step-box-text").slideUp();
        }
        $(target).slideToggle();
        $('html,body').animate({
            scrollTop: $('.mobile-3-steps').offset().top
        });

    });

    //slider on front_page
    $(".owl-1").owlCarousel({
        responsive: {
            0: {
                items: 1
            }
        },
        loop: true,
        // margin: 10,
        autoplay: 2000,
        stopOnHover: true,
        smartSpeed: 1000,
        slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        dots: true,
        // autoHeight: true
    });


    ///slider na main-page- główny, na sekcje 100vh
    // if ($(window).width() > 1360) {

    //     $(".owl-big").owlCarousel({

    //         responsive: {
    //             0: {
    //                 items: 1
    //             }
    //         },
    //         autoplay: 2000,
    //         animateOut: 'slideOutUp',
    //         animateIn: 'slideInUp',
    //         dots: false,
    //         smartSpeed: 850,
    //         slideTransition: 'ease-in-out',

    //     });
    // }



    //slider partnerzy

    $(".owl-2").owlCarousel({

        responsive: {
            0: {
                items: 3,
                margin: 20
            },
            640: {
                items: 4,
                margin: 40
            },
            830: {
                items: 4,
                margin: 60
            },
            1200: {
                items: 4,
                margin: 100
            }
        },
        loop: true,

        autoplay: 2000,
        stopOnHover: true,
        smartSpeed: 850,
        slideTransition: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
        nav: true,
        // autoplayTimeout:1000,
        dots: false,
        autoHeight: true
    });

    ////animation on your_home
    $(' .sun-animation .sun').click(function (e) {
        e.preventDefault()
        let element_id = $(this).attr('data-show-panel');
        let target = ".sun-text" + element_id;
        $(target).show();
        $(this).addClass("color-blue-2");
        $(this).siblings().removeClass("color-blue-2");
        $(target).siblings().hide();
    })

    //mobile
    $(".sun-animation-mobile .sun").click(function (e) {
        e.preventDefault()
        let element_id = $(this).attr('data-show-panel');
        let target = ".sun-text" + element_id;

        if ($(".animated-ul-mobile:visible").not($(target)).length) {
            $(".animated-ul-mobile").slideUp();
            $(this).siblings().removeClass('color-blue-2');
        }
        $(this).toggleClass("color-blue-2");
        $(target).slideToggle();

        $('html,body').animate({
            scrollTop: $('.animation-containter').offset().top
        });
    });


    //house animation-mobile
    $('.shadow-ball-animation').click(function (e) {
        e.preventDefault()
        if ($(window).width() < 641) {
            let element_id = $(this).attr('data-show-tooltip');
            let target = "#tooltip-" + element_id;
            $('.tooltiptext-mobile:visible').hide();
            $(target).show();
            $('html,body').animate({
                scrollTop: $(target).offset().top
            });
            $(target).click(function () {
                $(target).fadeOut(400);
            })
        }
    })

    //wersja od Tomka- up 980px

    // $('[data-show-panel]').click(function(){
    //     var targetSelector = $(this).attr('data-show-panel'); 
    //     if( targetSelector && $(targetSelector).length ){

    //         if( $(targetSelector).is(':visible') ) return; // Już widoczny

    //         //Jeśli mamy inne widoczne
    //         if( $('.animated-ul:visible').not( $(targetSelector) ).length ){
    //             // Ukryj inne, potem pokaż ten
    //             $('.animated-ul:visible').not( $(targetSelector) ).stop().fadeOut(400, function(){
    //                 $(targetSelector).stop().fadeIn();
    //             });
    //         }
    //         // Nie mamy innych widocznych
    //         else {
    //             // Pokaż ten	
    //             $(targetSelector).stop().fadeIn();
    //         }

    //     }
    // })

    ///toggle job offers
    $(".offer").click(function (e) {
        e.preventDefault()
        let id_faq = $(this).attr('data-show-offer');
        let target = '.offer-text-' + id_faq;
        if ($(".offer-text:visible").not($(target)).length) {
            $(".offer-text").slideUp();
            $(this).siblings().removeClass('offer-added-class');
        }

        $(this).toggleClass('offer-added-class');
        $(target).slideToggle();

        $('html,body').animate({
            scrollTop: $('.offers-title').offset().top
        }, 'slow');
    });


    /////form-toggle
    $("#consul-starp").click(function (e) {
        e.preventDefault()
        // let element_id = $(this).attr('data-show-panel');
        // let target = ".sun-text" + element_id;

        // if( $(".animated-ul-mobile:visible").not($(target)).length ) {
        //     $(".animated-ul-mobile").slideUp();
        //     $(this).siblings().removeClass('color-blue-2');
        // }
        $('.my-form .form-more-strap svg').toggleClass("transform-180");
        $(this).toggleClass("added-form-class");
        $(".additional").slideToggle('easeOutCirc');
        $(".back-4").toggle();


        $('html,body').animate({
            scrollTop: $(this).offset().top
        }, 500, "easeOutCirc");
    });

    ///hamburger
    $('#hamburger').click(function () {
        if (!Foundation.MediaQuery.atLeast(980)) {
            $('.menu-wrap').toggleClass('active');
        }
    });
});